<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define('DB_NAME', 'wp_tinhdevcms');

/** Username của database */
define('DB_USER', 'root');

/** Mật khẩu của database */
define('DB_PASSWORD', '');

/** Hostname của database */
define('DB_HOST', 'localhost');

/** Database charset sử dụng để tạo bảng database. */
define('DB_CHARSET', 'utf8mb4');

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bav&|z2a8G-!]rHf@OqP+5.Aqv4##-kvWZ-)V5Gm+Qj.$P)73}e8o8mMG,lPRP:/');
define('SECURE_AUTH_KEY',  'DCN_T#Ue-Ng -p]#umW&#_$84`e1r<b7vs7v}y/-DkSp#HoBgq|K-vyzppeq;Y(y');
define('LOGGED_IN_KEY',    'N}H$|5(`}g 1aB7JcH|)A2+47(|(E9-I|]Ol68-KWD,|Pp@/T6o,1 r|A$r LRCH');
define('NONCE_KEY',        'RU#vHoC9J;j#B)HkY8245qg|R/hC6zUp`fNg`TB?rin2U!I,l6T|^5l1_*N%LxzG');
define('AUTH_SALT',        'g^tRafJ,`l.i7^HcUN|Rw3mCcRH8644rB#_z47DK }!`5<t~>rq$8uM X`/y[y,#');
define('SECURE_AUTH_SALT', '2[qmZ82d$8 Xx21Wzva.VcLo5AMc5qn$zWE.b])pmQx9]--;+rCF ^p~D|D,3R6-');
define('LOGGED_IN_SALT',   'I*;|9qwZh5$KC071m]7KKIf&*J24@ |{Jgkm3`gG*f-O*U4l s@8%7i4F2~4y8`z');
define('NONCE_SALT',       'z2~=:6{AFJFgrN`<mVD<7F|zy2oIt!/:)9}c`Ct|W::iE5l/s|^|._9)BQ|p)-?&');

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'tdv_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
