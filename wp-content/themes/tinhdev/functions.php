<?php

/**
 * defines
 */
define('TINDEV_VERSION', '1.0.0');

/**
 * Start the engine - Genesis Framework (do not remove).
 */
require_once get_template_directory() . '/lib/init.php';
/**
 * Start the engine - Tinh Dev Theme
 */
require_once 'includes/TinhDev.php';
require_once 'public/functions.php';
if (class_exists('WooCommerce')):
	require_once 'public/woocommerce-functions.php';
endif;

/**
 * Start the theme
 */
function runTinhDev(){
	$theme = new TinhDev();
	$theme->run();
}

runTinhDev();

/*
 * Bật Classic Editor không cần plugin
 * */
//add_filter('use_block_editor_for_post', '__return_false');

/*
 * Tắt XMLRPC
 * */
add_filter('xmlrpc_enabled', '__return_false');