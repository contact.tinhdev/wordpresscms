<?php

/**
 * Class TinhDev
 */
class TinhDev
{
    protected $loader;
    protected $theme_name;
    protected $version;

    public function __construct()
    {
        if (defined('TINHDEV_VERSION')) {
            $this->version = TINHDEV_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->theme_name = 'tinhdev';
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_public_hooks();
        $this->remove_emoji_from_wp();
    }

    private function load_dependencies()
    {
        require_once get_theme_file_path() . '/includes/TinhDevLoader.php';
        require_once get_theme_file_path() . '/admin/TinhDevAdmin.php';
        require_once get_theme_file_path() . '/public/TinhDevPublic.php';
        require_once get_theme_file_path() . '/admin/partials/acf-option-page.php';
        require_once get_theme_file_path() . '/public/layouts.php';
        $this->loader = new TinhDevLoader();
    }

    private function define_admin_hooks()
    {
        $theme_admin = new TinhDevAdmin($this->get_theme_name(), $this->get_version());
        remove_action('welcome_panel', 'wp_welcome_panel');
        $this->loader->add_action('admin_enqueue_scripts', $theme_admin, 'enqueue_styles');
        $this->loader->add_action('admin_head', $theme_admin, 'favicon');
        $this->loader->add_action('admin_head', $theme_admin, 'remove_help_tabs');
        $this->loader->add_action('welcome_panel', $theme_admin, 'welcome_panel');
        $this->loader->add_action('admin_menu', $theme_admin, 'remove_genesis_scripts_box');
        $this->loader->add_action('admin_menu', $theme_admin, 'disable_dashboard_widgets');
        $this->loader->add_action('acf/init', $theme_admin, 'acf_option_page');
        $this->loader->add_action('admin_init', $theme_admin, 'add_caps');
        $this->loader->add_filter('screen_options_show_screen', $theme_admin, 'remove_screen_options');
        $this->loader->add_filter('admin_footer_text', $theme_admin, 'footer_text', 11);
        $this->loader->add_filter('update_footer', $theme_admin, 'footer_update', 11);
        $this->loader->add_filter('get_the_archive_title', $theme_admin, 'replace_archive_title', 11);
    }

    private function define_public_hooks()
    {

        $theme_public = new TinhDevPublic($this->get_theme_name(), $this->get_version());
        $this->loader->add_action('login_enqueue_scripts', $theme_public, 'login_template');
        $this->loader->add_action('login_headertext', $theme_public, 'login_logo_title');
        $this->loader->add_action('login_headerurl', $theme_public, 'login_logo_url');
        $this->loader->add_action('after_setup_theme', $theme_public, 'theme_support');
        $this->loader->add_action('wp_enqueue_scripts', $theme_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $theme_public, 'enqueue_scripts');
        $this->loader->add_action('wp_head', $theme_public, 'google_analytics');
    }

    private function remove_emoji_from_wp()
    {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
    }

    public function run()
    {
        $this->loader->run();
    }

    public function get_theme_name()
    {
        return $this->theme_name;
    }

    public function get_loader()
    {
        return $this->loader;
    }

    public function get_version()
    {
        return $this->version;
    }
}
