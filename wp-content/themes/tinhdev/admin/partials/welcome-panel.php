<div class="welcome-panel-content">
    <h2>Xin chào! Bạn đã đăng nhập vào khu vực Quản trị của TinhDev!</h2>
    <div class="welcome-panel-column-container">
        <div class="welcome-panel-column">
            <h3>Các Bước Tiếp Theo</h3>
            <ul>
                <?php if ('page' == get_option('show_on_front') && !get_option('page_for_posts')) : ?>
                    <li><?php printf('<a href="%s" class="welcome-icon welcome-edit-page">' . __('Edit your front page') . '</a>', get_edit_post_link(get_option('page_on_front'))); ?></li>
                    <li><?php printf('<a href="%s" class="welcome-icon welcome-add-page">' . __('Add additional pages') . '</a>', admin_url('post-new.php?post_type=page')); ?></li>
                <?php elseif ('page' == get_option('show_on_front')) : ?>
                    <li><?php printf('<a href="%s" class="welcome-icon welcome-edit-page">' . __('Edit your front page') . '</a>', get_edit_post_link(get_option('page_on_front'))); ?></li>
                    <li><?php printf('<a href="%s" class="welcome-icon welcome-add-page">' . __('Add additional pages') . '</a>', admin_url('post-new.php?post_type=page')); ?></li>
                    <li><?php printf('<a href="%s" class="welcome-icon welcome-write-blog">' . __('Add a blog post') . '</a>', admin_url('post-new.php')); ?></li>
                <?php else : ?>
                    <li><?php printf('<a href="%s" class="welcome-icon welcome-write-blog">' . __('Write your first blog post') . '</a>', admin_url('post-new.php')); ?></li>
                    <li><?php printf('<a href="%s" class="welcome-icon welcome-add-page">' . __('Add an About page') . '</a>', admin_url('post-new.php?post_type=page')); ?></li>
                    <li><a href="<?= home_url('/wp-admin/admin.php?page=website-settings'); ?>"
                           class="welcome-icon welcome-setup-home">Cài đặt trang chủ</a></li>
                <?php endif; ?>
                <li><?php printf('<a href="%s" class="welcome-icon welcome-view-site">' . __('View your site') . '</a>', home_url('/')); ?></li>
            </ul>
        </div>

        <div class="welcome-panel-column-img">
            <img src="<?= get_theme_file_uri() ?>/admin/images/tinhdev.jpg" alt="">
        </div>
    </div>
</div>
