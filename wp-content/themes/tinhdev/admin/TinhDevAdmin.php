<?php

/**
 * Class TinhDevAdmin
 */
class TinhDevAdmin
{
    private $theme_name;
    private $version;

    /**
     * @param $theme_name
     * @param $version
     */
    public function __construct($theme_name, $version)
    {
        $this->theme_name = $theme_name;
        $this->version = $version;
        $this->genesis_remove_theme_support();
        remove_action('admin_menu', 'genesis_add_inpost_seo_box');
    }

    /**
     * Genesis remove theme support
     */
    function genesis_remove_theme_support()
    {
        remove_theme_support('genesis-admin-menu');
        remove_theme_support('genesis-inpost-layouts');
    }

    function remove_genesis_scripts_box()
    {
        $types = array('post', 'page');
        remove_meta_box('genesis_inpost_scripts_box', $types, 'normal');
    }

    public function enqueue_styles()
    {
        wp_enqueue_style($this->theme_name, get_theme_file_uri() . '/admin/css/style.css', array(), $this->version, 'all');
        if (!current_user_can('administrator')):
            wp_enqueue_style($this->theme_name . '-not-administrator', get_theme_file_uri() . '/admin/css/not-administrator.css', array(), $this->version, 'all');
        endif;
    }

    function favicon()
    {
        $favicon = get_theme_file_uri() . '/admin/images/favicon.png';
        echo '<link rel="shortcut icon" href="' . $favicon . '" />';
    }

    function remove_help_tabs()
    {
        if (!current_user_can('administrator')) {
            $screen = get_current_screen();
            $screen->remove_help_tabs();
        }
    }

    function remove_screen_options()
    {
        $screen = get_current_screen();
        if ($screen->id == 'dashboard') {
            if (!current_user_can('administrator')) {
                return false;
            }
        }
        return true;
    }

    function disable_dashboard_widgets()
    {
        if (!current_user_can('administrator')) {
            remove_meta_box('dashboard_right_now', 'dashboard', 'normal');// Remove "At a Glance"
            remove_meta_box('dashboard_activity', 'dashboard', 'normal');// Remove "Activity" which includes "Recent Comments"
            remove_meta_box('dashboard_quick_press', 'dashboard', 'side');// Remove Quick Draft
            remove_meta_box('dashboard_primary', 'dashboard', 'core');// Remove WordPress Events and News
        }
    }

    function footer_text()
    {
        echo '<span id="footer-thankyou">Cảm ơn bạn đã sử dụng dịch vụ của <a href="https://tinhdev.com" target="_blank">Tinh Dev</a></span>';
    }

    function footer_update()
    {
        echo '<span>Phiên bản ' . $this->version . '</span>';
    }

    function welcome_panel()
    {
        require_once get_theme_file_path() . '/admin/partials/welcome-panel.php';
    }

    function acf_option_page()
    {
        if (function_exists('acf_add_options_page')):
            $parent = acf_add_options_page(array(
                'page_title' => __('Cài đặt website'),
                'menu_title' => __('Cài đặt website'),
                'menu_slug' => 'tinh-dev',
                'redirect' => true,
            ));

            acf_add_options_sub_page(array(
                'page_title' => 'Cài đặt chung',
                'menu_title' => 'Cài đặt chung',
                'menu_slug' => 'theme-general-settings',
                'parent_slug' => $parent['menu_slug'],
                'update_button' => __('Cập nhật', 'tinhdev'),
                'updated_message' => __("Cập nhật thành công", 'tinhdev')
            ));
        endif;
    }

    function add_caps()
    {
        if (class_exists('WooCommerce') && is_admin()):
            $role = get_role('shop_manager');
            $role->add_cap('unfiltered_html');
        endif;
    }

    function replace_archive_title($title)
    {
        if (is_category()) {
            $title = single_cat_title('', false);
        } elseif (is_tag()) {
            $title = single_tag_title('', false);
        } elseif (is_author()) {
            $title = '<span class="vcard">' . get_the_author() . '</span>';
        } elseif (is_post_type_archive()) {
            $title = post_type_archive_title('', false);
        } elseif (is_tax()) {
            $title = single_term_title('', false);
        }
        return $title;
    }
}
