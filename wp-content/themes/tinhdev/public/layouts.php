<?php

/**
 *
 */
add_action('genesis_header', 'header_components');

/**
 *
 */
function header_components()
{

}

/**
 *
 */
add_action('genesis_loop', 'home_components');

/**
 *
 */
function home_components()
{
    if (is_home()):

    endif;
}

/**
 *
 */
add_action('genesis_loop', 'single_components');

/**
 *
 */
function single_components()
{
    if (is_single()):
        if (class_exists('WooCommerce') && is_woocommerce()):
            /**
             * single product
             */
        else:
            /**
             * single
             */
        endif;
    endif;
}

/**
 *
 */
add_action('genesis_loop', 'page_components');

/**
 *
 */
function page_components()
{
    if (is_page() && !is_page_template()):

    endif;
}

/**
 *
 */
add_action('genesis_loop', 'archive_components');

/**
 *
 */
function archive_components()
{
    if (is_archive()):
        if (class_exists('WooCommerce') && is_woocommerce()):
            /**
             * archive product
             */
        else:
            /**
             * archive
             */
        endif;
    endif;
}

/**
 *
 */
add_action('genesis_loop', 'search_components');

/**
 *
 */
function search_components()
{
    if (is_search()):
        if (class_exists('WooCommerce') && is_woocommerce()):
            /**
             * archive product
             */
        else:
            /**
             * archive
             */
        endif;
    endif;
}

/**
 *
 */
add_action('genesis_loop', 'page_not_found_components');

/**
 *
 */
function page_not_found_components()
{
    if (is_404()):

    endif;
}

/**
 *
 */
add_action('genesis_footer', 'footer_components');

/**
 *
 */
function footer_components()
{

}
