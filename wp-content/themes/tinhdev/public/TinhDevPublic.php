<?php

/**
 * Class TinhDevPublic
 */
class TinhDevPublic{

	private $theme_name;
	private $version;

	/**
	 * @param $theme_name
	 * @param $version
	 */
	public function __construct($theme_name, $version){
		$this->theme_name = $theme_name;
		$this->version    = $version;
		$this->theme_structure();
		$this->remove_genesis_hooks();
	}

	public function login_template(){
		wp_enqueue_style($this->theme_name . '-login',
			get_theme_file_uri() . '/public/css/login-template.css', [], $this->version, 'all');
	}

	public function enqueue_styles(){
		wp_enqueue_style('bootstrap',
			get_theme_file_uri() . '/public/component-assets/lib/bootstrap/css/bootstrap.min.css',
			[], 'v4.5.2', 'all');
		wp_enqueue_style('mmenu',
			get_theme_file_uri() . '/public/component-assets/lib/mmenu-js/mmenu.css', [], '8.5.18',
			'all');
		wp_enqueue_style('slick',
			get_theme_file_uri() . '/public/component-assets/lib/slick/slick.css', [], '1.8.1',
			'all');
		wp_enqueue_style('slick-theme',
			get_theme_file_uri() . '/public/component-assets/lib/slick/slick-theme.css', [],
			'1.8.1', 'all');
		wp_enqueue_style($this->theme_name . '-style',
			get_theme_file_uri() . '/public/component-assets/css/style.css', [], $this->version,
			'all');
	}

	public function enqueue_scripts(){
		wp_enqueue_script('bootstrap',
			get_theme_file_uri() . '/public/component-assets/lib/bootstrap/js/bootstrap.min.js',
			['jquery'], 'v4.5.2', TRUE);
		wp_enqueue_script('mmenu-polyfillss',
			get_theme_file_uri() . '/public/component-assets/lib/mmenu-js/mmenu.polyfills.js',
			['jquery'], '8.5.18', TRUE);
		wp_enqueue_script('mmenu',
			get_theme_file_uri() . '/public/component-assets/lib/mmenu-js/mmenu.js', ['jquery'],
			'8.5.18', TRUE);
		wp_enqueue_script('slick',
			get_theme_file_uri() . '/public/component-assets/lib/slick/slick.min.js', ['jquery'],
			'1.8.1', TRUE);
		wp_enqueue_script($this->theme_name . '-script',
			get_theme_file_uri() . '/public/component-assets/js/script.js', ['jquery'],
			$this->version, TRUE);
	}

	private function login_logo_title(){
		return 'Tinh Dev';
	}

	private function login_logo_url(){
		return 'https://tinhdev.com';
	}

	public function theme_support(){

		/**
		 * Enable responsive viewport.
		 */
		add_theme_support('genesis-responsive-viewport');

		/**
		 * Enable automatic output of WordPress title tags.
		 */
		add_theme_support('title-tag');

		/**
		 * Enable HTML5 markup structure.
		 */
		add_theme_support('html5', [
			'caption',
			'comment-form',
			'comment-list',
			'gallery',
			'search-form',
		]);

		/**
		 * Enable WooCommerce support.
		 */
		if (class_exists('WooCommerce')):
			add_theme_support('woocommerce');
		endif;
	}

	private function theme_structure(){
		/**
		 * Remove primary sidebar.
		 */
		unregister_sidebar('sidebar');
		remove_action('genesis_sidebar', 'genesis_do_sidebar');

		/**
		 * Remove secondary sidebar.
		 */
		unregister_sidebar('sidebar-alt');
		remove_action('genesis_sidebar_alt', 'genesis_do_sidebar_alt');

		/**
		 * Remove header right widget area.
		 */
		unregister_sidebar('header-right');
		remove_action('genesis_header', 'genesis_do_header');

		/**
		 * Set full width content
		 */
		add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

		/**
		 * Remove genesis menu
		 */
		remove_theme_support('genesis-menus');
	}

	/**
	 *
	 */
	private function remove_genesis_hooks(){
		/**
		 * Remove loop
		 */
		remove_action('genesis_loop', 'genesis_do_loop');

		/**
		 * Remove footer
		 */
		remove_action('genesis_footer', 'genesis_do_footer');
	}


	public function google_analytics(){
		$theme_general_analytics_id = get_field('theme_general_analytics_id', 'option');
		if ($theme_general_analytics_id != ''): ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $theme_general_analytics_id ?>"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());
                gtag('config', '<?= $theme_general_analytics_id ?>');
            </script>
		<?php endif;
	}
}
